import json

import pika

import settings
from rabbit.base.basic_rabbit_client import RabbitClient


class RabbitProducer(RabbitClient):
    def __init__(self):
        super().__init__(
            settings.RABBITMQ_HOST,
            settings.RABBITMQ_PORT,
            settings.RABBITMQ_USER,
            settings.RABBITMQ_PASSWORD,
            settings.RABBITMQ_VIRTUAL_HOST,
        )
        self.__reg_apps_channel = None

        self.__exchange_name = 'registration_direct'
        self.__reg_app_queue_name = 'reg_apps'

    def __declare_reg_apps_queue(self):
        self.__reg_apps_channel = self._connection.channel()
        self.__reg_apps_channel.exchange_declare(exchange=self.__exchange_name, exchange_type='direct', durable=True)
        self.__reg_apps_channel.queue_declare(queue=self.__reg_app_queue_name, durable=True)

    def public_to_reg_apps_queue(self, body: dict):
        self._create_connection()
        self.__declare_reg_apps_queue()
        self.__reg_apps_channel.basic_publish(
            exchange=self.__exchange_name,
            routing_key=self.__reg_app_queue_name,
            body=json.dumps(body),
        )
        self._connection.close()
