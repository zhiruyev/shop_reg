import settings
from auth_service import AuthService
from entities.register_user_input_entity import InputRegisterUserEntity
from rabbit.base.basic_consumer import BasicConsumer


class RegAppConsumer(BasicConsumer):
    def __init__(self):
        super().__init__(
            settings.RABBITMQ_HOST,
            settings.RABBITMQ_PORT,
            settings.RABBITMQ_USER,
            settings.RABBITMQ_PASSWORD,
            settings.RABBITMQ_VIRTUAL_HOST,
            'registration_direct',
            'reg_apps',
        )

    @staticmethod
    def _callback_func(data: dict):
        AuthService().reg_user(
            InputRegisterUserEntity(
                username=data['username'],
                password=data['password'],
            )
        )
