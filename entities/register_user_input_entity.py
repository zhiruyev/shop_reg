from pydantic import BaseModel


class InputRegisterUserEntity(BaseModel):
    username: str
    password: str
