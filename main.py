import uvicorn
from fastapi import FastAPI, APIRouter
from starlette.requests import Request
from starlette.responses import Response

from entities.register_user_input_entity import InputRegisterUserEntity
from rabbit.producer import RabbitProducer

app = FastAPI(
    docs_url="/docs",
    openapi_url="/openapi.json",
)
main_router = APIRouter()


@main_router.post(
    "/register_user",
    tags=["registration"],
)
def register_user(
    request: Request,
    item: InputRegisterUserEntity,
):
    public_message = {"username": item.username, "password": item.password}
    RabbitProducer().public_to_reg_apps_queue(public_message)

    return Response()


app.include_router(main_router)

if __name__ == '__main__':
    uvicorn.run(app, host="localhost", port=8099)
